<?php

declare(strict_types=1);

namespace Drupal\pinto\ThemeDefinition;

/**
 * Represents a hook_theme definition.
 *
 * @internal
 */
final class HookThemeDefinition {

  /**
   * @phpstan-param array<string, mixed> $definition
   */
  public function __construct(
    public array $definition,
  ) {
  }

}
