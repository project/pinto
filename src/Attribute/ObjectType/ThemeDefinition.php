<?php

declare(strict_types=1);

namespace Drupal\pinto\Attribute\ObjectType;

use Pinto\Attribute\ObjectType\LegacyThemeDefinitionTrait;
use Pinto\ObjectType\ObjectTypeInterface;

/**
 * An attribute representing the theme definition.
 *
 * - When attached to a class, the value of $definition must be set.
 * - When attached to a method, the $definition must not be set, the definition
 *   is instead returned by the method.
 *
 * The definition is merged into the default hook_theme definition.
 *
 * There is no need to define `template` or `path` key, as these are provided.
 * Though you may choose to override.
 *
 * This class replaces \Drupal\pinto\Attribute\ObjectType\ThemeDefinition.
 */
#[\Attribute(flags: \Attribute::TARGET_CLASS | \Attribute::TARGET_METHOD)]
final class ThemeDefinition implements ObjectTypeInterface {

  // Code in this trait should be moved to this Drupal version when it is
  // fully removed from Pinto lib.
  use LegacyThemeDefinitionTrait;

}
