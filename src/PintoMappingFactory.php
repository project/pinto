<?php

declare(strict_types=1);

namespace Drupal\pinto;

use Pinto\PintoMapping;

/**
 * Pinto Mapping Factory.
 *
 * Internal factory for creating mapping objects. This is used since Drupal's
 * container (\Drupal\Component\DependencyInjection\Dumper\OptimizedPhpArrayDumper::dumpValue)
 * is incapable of having PHP objects as parameters.
 * Throws "Unable to dump a service container if a parameter is an object
 * without _serviceId.".
 *
 * @internal
 */
final class PintoMappingFactory {

  private array $definitions;

  public function __construct(
    private array $enumClasses,
    private array $enums,
    string $definitions,
    private array $buildInvokers,
    private array $types,
  ) {
    // From \Drupal\pinto\PintoCompilerPass.
    // @phpstan-ignore-next-line
    $this->definitions = \unserialize($definitions);
  }

  public function create(): PintoMapping {
    return new PintoMapping(
      $this->enumClasses,
      $this->enums,
      $this->definitions,
      $this->buildInvokers,
      $this->types,
    );
  }

}
