<?php

declare(strict_types=1);

namespace Drupal\pinto\Object;

use Pinto\List\ObjectListInterface;
use Pinto\Slots;

/**
 * Transforms Pinto-lib specific builds to Drupal render arrays.
 *
 * @internal
 *   For internal use only.
 */
final class PintoToDrupalBuilder {

  public static function transform(mixed $built, ObjectListInterface $case, callable $lazyDefinition): mixed {
    // Transform \Pinto\Attribute\ObjectType\Slots build to Drupal render array.
    if ($built instanceof Slots\Build) {
      $definition = $lazyDefinition();
      \assert($definition instanceof Slots\Definition);

      $build = [
        '#theme' => $case->name(),
        '#attached' => ['library' => $case->attachLibraries()],
      ];

      foreach ($definition->slots as $slot) {
        $build['#' . static::unitEnumToHookThemeVariableName($slot->name)] = $built->pintoGet($slot->name);
      }

      return $build;
    }

    return $built;
  }

  public static function unitEnumToHookThemeVariableName(\UnitEnum|string $unitEnumOrString): string {
    if ($unitEnumOrString instanceof \UnitEnum) {
      // An enum case name is always valid.
      return $unitEnumOrString->name;
    }

    // According to hook_theme() docs a valid hook_theme variable name must be
    // both a valid PHP and Twig variable name:
    // "(so they must be legal PHP/Twig variable names)".
    return \preg_replace(
      // This regex is effectively Lexer::REGEX_NAME but with negates (^).
      '/[^a-zA-Z_\x7f-\xff][^a-zA-Z0-9_\x7f-\xff]*/',
      '',
      $unitEnumOrString,
    ) ?? throw new \Exception('Failed to transform to variable name.');
  }

}
