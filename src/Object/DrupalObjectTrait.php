<?php

declare(strict_types=1);

namespace Drupal\pinto\Object;

use Pinto\Object\ObjectTrait;
use Pinto\PintoMapping;

trait DrupalObjectTrait {

  use ObjectTrait {
    pintoBuild as originalPintoBuild;
  }

  private function pintoBuild(callable $wrapper): mixed {
    $built = $this->originalPintoBuild($wrapper);
    return PintoToDrupalBuilder::transform($built, static::$pintoEnum[static::class], fn (): mixed => $this->pintoMapping()->getThemeDefinition($this::class));
  }

  /**
   * Use a mapping pre-made in the container.
   *
   * @internal
   */
  private function pintoMapping(): PintoMapping {
    return \Drupal::service(PintoMapping::class);
  }

}
