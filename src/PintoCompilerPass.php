<?php

declare(strict_types=1);

namespace Drupal\pinto;

use Drupal\pinto\Object\PintoToDrupalBuilder;
use Drupal\pinto\ThemeDefinition\HookThemeDefinition;
use Pinto\Attribute\Build;
use Pinto\Attribute\Definition;
use Pinto\List\ObjectListInterface;
use Pinto\ObjectType\ObjectTypeDiscovery;
use Pinto\Slots\Definition as SlotsDefinition;
use Pinto\Slots\NoDefaultValue;
use Pinto\ThemeDefinition\HookThemeDefinition as PintoHookThemeDefinition;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Pinto compiler pass.
 *
 * Searches for Pinto enums in defined namespaces, then records the classnames
 * to the container so they may be efficiently fetched at runtime.
 */
final class PintoCompilerPass implements CompilerPassInterface {

  public function process(ContainerBuilder $container): void {
    /** @var array<class-string, string> $containerNamespaces */
    $containerNamespaces = $container->getParameter('container.namespaces');

    /** @var string[] $pintoNamespaces */
    $pintoNamespaces = $container->getParameter('pinto.namespaces');

    $enumClasses = [];
    $enums = [];
    $definitions = [];
    $buildInvokers = [];
    $types = [];

    foreach ($this->getEnums($containerNamespaces, $pintoNamespaces) as $r) {
      /** @var class-string<\Pinto\List\ObjectListInterface> $objectListClassName */
      $objectListClassName = $r->getName();
      $enumClasses[] = $objectListClassName;

      foreach ($r->getReflectionConstants() as $constant) {
        /** @var array<\ReflectionAttribute<\Pinto\Attribute\Definition>> $attributes */
        $attributes = $constant->getAttributes(Definition::class);
        $definition = ($attributes[0] ?? NULL)?->newInstance();
        if ($definition === NULL) {
          continue;
        }

        if ($container->hasDefinition($definition->className)) {
          // Ignore when this is a service.
          continue;
        }

        /** @var \Pinto\List\ObjectListInterface $case */
        $case = $constant->getValue();
        $themeObjectDefinition = ObjectTypeDiscovery::definitionForThemeObject($definition->className, $case);
        $types[$definition->className] = $themeObjectDefinition[0];
        $definitions[$definition->className] = $themeObjectDefinition[1];
        $buildInvokers[$definition->className] = Build::buildMethodForThemeObject($definition->className);
        $enums[$definition->className] = [$objectListClassName, $constant->getName()];
      }
    }

    $container->getDefinition(PintoMappingFactory::class)
      // $enumClasses is a separate parameter since there may be zero $enums.
      ->setArgument('$enumClasses', $enumClasses)
      ->setArgument('$enums', $enums)
      ->setArgument('$definitions', \serialize($definitions))
      ->setArgument('$buildInvokers', $buildInvokers)
      ->setArgument('$types', $types);

    // Precompile hook_theme().
    $container->setParameter('pinto.internal.hook_theme', \array_merge(...\array_map(static function (string $enumClass) {
      /** @var class-string<\Pinto\List\ObjectListInterface> $enumClass */
      $definitions = [];
      $enumDefinitions = $enumClass::definitions();
      foreach ($enumDefinitions as $case) {
        $definition = $enumDefinitions[$case];

        $definitions[$case->name()] = match (TRUE) {
          // ThemeDefinition allows objects to define a hook_theme in full.
          $definition instanceof HookThemeDefinition => $definition->definition,
          $definition instanceof PintoHookThemeDefinition => $definition->definition,
          // Slots are a simplified (and recommended) since most objects don't
          // care about Drupal hook_theme-isms.
          $definition instanceof SlotsDefinition => ((static function () use ($case, $definition): array {
            $hookTheme = [
              'variables' => [],
              'path' => $case->templateDirectory(),
              'template' => $case->templateName(),
            ];

            // The keys of these are mapped at runtime in
            // \Drupal\pinto\Object\PintoToDrupalBuilder::transform().
            foreach ($definition->slots as $slot) {
              // Use null for when a slot has no default value.
              $hookTheme['variables'][PintoToDrupalBuilder::unitEnumToHookThemeVariableName($slot->name)] = $slot->defaultValue instanceof NoDefaultValue ? NULL : $slot->defaultValue;
            }

            return $hookTheme;
          })()),
          default => throw new \LogicException(\sprintf('Unhandled definition for %s', $case->name))
        };
      }
      return $definitions;
    }, $enumClasses)));
  }

  /**
   * Get enums for the provided namespaces.
   *
   * @param array<class-string, string> $namespaces
   *   An array of namespaces. Where keys are class strings and values are
   *   paths.
   * @param string[] $pintoNamespaces
   *   Pinto namespaces.
   *
   * @return \Generator<\ReflectionClass<\Pinto\List\ObjectListInterface>>
   *   Generates class strings.
   *
   * @throws \ReflectionException
   */
  private function getEnums(array $namespaces, array $pintoNamespaces): \Generator {
    foreach ($namespaces as $namespace => $dirs) {
      $dirs = (array) $dirs;
      foreach ($dirs as $dir) {
        foreach ($pintoNamespaces as $pintoNamespace) {
          $nsDir = $dir . '/' . \str_replace('\\', '/', $pintoNamespace);
          if (!\file_exists($nsDir)) {
            continue;
          }
          $namespace .= '\\' . $pintoNamespace;

          /** @var \RecursiveIteratorIterator<\RecursiveDirectoryIterator<\SplFileInfo>> $iterator */
          $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($nsDir, \FilesystemIterator::KEY_AS_PATHNAME | \FilesystemIterator::CURRENT_AS_FILEINFO | \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST);
          foreach ($iterator as $fileinfo) {
            \assert($fileinfo instanceof \SplFileInfo);
            if ('php' !== $fileinfo->getExtension()) {
              continue;
            }

            /** @var \RecursiveDirectoryIterator|null $subDir */
            $subDir = $iterator->getSubIterator();
            if (NULL === $subDir) {
              continue;
            }

            $subDir = $subDir->getSubPath();
            $subDir = $subDir !== '' ? \str_replace(DIRECTORY_SEPARATOR, '\\', $subDir) . '\\' : '';

            /** @var class-string $class */
            $class = $namespace . '\\' . $subDir . $fileinfo->getBasename('.php');

            /** @var \ReflectionClass<\Pinto\List\ObjectListInterface> $reflectionClass */
            $reflectionClass = new \ReflectionClass($class);
            if ($reflectionClass->isEnum() && $reflectionClass->implementsInterface(ObjectListInterface::class)) {
              yield $reflectionClass;
            }
          }
        }
      }
    }
  }

}
