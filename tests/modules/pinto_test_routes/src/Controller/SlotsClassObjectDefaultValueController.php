<?php

declare(strict_types=1);

namespace Drupal\pinto_test_routes\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\pinto_test\Pinto\Slots\SlotsObjectDefaultValue;

final class SlotsClassObjectDefaultValueController extends ControllerBase {

  public function __invoke(): array {
    return [
      'foo' => (new SlotsObjectDefaultValue('Text!'))(),
    ];
  }

}
