<?php

declare(strict_types=1);

namespace Drupal\pinto_test_routes\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\pinto_test\Pinto\Slots\SlotsObjectBindPromotedPublic;

final class SlotsObjectBindPromotedPublicController extends ControllerBase {

  public function __invoke(): array {
    return [
      'foo' => (new SlotsObjectBindPromotedPublic(pub: 'Public text!'))(),
    ];
  }

}
