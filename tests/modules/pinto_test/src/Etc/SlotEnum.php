<?php

declare(strict_types=1);

namespace Drupal\pinto_test\Etc;

/**
 * A test enum for slot names.
 */
enum SlotEnum {
  case Slot1;
  case Slot2;
  case Slot3;
}
