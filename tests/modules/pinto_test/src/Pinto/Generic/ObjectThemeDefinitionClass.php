<?php

declare(strict_types=1);

namespace Drupal\pinto_test\Pinto\Generic;

use Drupal\pinto_test\Other\OtherObject;
use Pinto\Attribute\Build;
use Pinto\Attribute\ThemeDefinition;

#[ThemeDefinition(
  definition: [
    'variables' => [
      'foo' => NULL,
      // Drupal is weird about objects in the container, test it explicitly.
      'other' => new OtherObject(),
    ],
  ],
)]
final class ObjectThemeDefinitionClass {

  #[Build]
  public function build(): mixed {
    return [];
  }

}
