<?php

declare(strict_types=1);

namespace Drupal\pinto_test\Pinto\Slots;

use Drupal\pinto\Object\DrupalObjectTrait;
use Drupal\pinto_test\Etc\SlotEnum;
use Pinto\Attribute\ObjectType\Slots;
use Pinto\Slots\Build;
use Pinto\Slots\Slot;

/**
 * Slots defined on the attribute, where slot names are enums.
 */
#[Slots(
  slots: [
    new Slot(name: SlotEnum::Slot1),
    new Slot(name: SlotEnum::Slot2, defaultValue: 3),
  ],
)]
final class SlotsObjectExplicitEnums {

  use DrupalObjectTrait;

  /**
   * Constructor.
   */
  public function __construct(
    readonly string $textShouldBeIgnored,
    readonly int $numberShouldBeIgnored = 4,
  ) {
  }

  public function __invoke(): mixed {
    return $this->pintoBuild(static function (Build $build): Build {
      return $build
        ->set(SlotEnum::Slot1, 'Slot One')
        ->set(SlotEnum::Slot2, 23456);
    });
  }

}
