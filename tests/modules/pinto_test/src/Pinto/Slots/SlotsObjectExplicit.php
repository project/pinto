<?php

declare(strict_types=1);

namespace Drupal\pinto_test\Pinto\Slots;

use Drupal\pinto\Object\DrupalObjectTrait;
use Pinto\Attribute\ObjectType\Slots;
use Pinto\Slots\Build;
use Pinto\Slots\Slot;

/**
 * Slots defined on the attribute, and constructor is not reflected.
 */
#[Slots(
  slots: [
    new Slot(name: 'text'),
    new Slot(name: 'number', defaultValue: 3),
  ],
)]
final class SlotsObjectExplicit {

  use DrupalObjectTrait;

  /**
   * Constructor.
   */
  public function __construct(
    readonly string $textShouldBeIgnored,
    readonly int $numberShouldBeIgnored = 4,
  ) {
  }

  public function __invoke(): mixed {
    return $this->pintoBuild(static function (Build $build): Build {
      return $build
        ->set('text', 'Some text')
        ->set('number', 12345);
    });
  }

}
