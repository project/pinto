<?php

declare(strict_types=1);

namespace Drupal\pinto_test\Pinto\Slots;

use Drupal\pinto\Object\DrupalObjectTrait;
use Pinto\Attribute\ObjectType\Slots;
use Pinto\Slots\Build;

/**
 * Tests slots type where a slot value is not set.
 */
#[Slots]
final class SlotsObjectDefaultValue {

  use DrupalObjectTrait;

  /**
   * Constructor.
   */
  public function __construct(
    readonly string $text,
    readonly int $number = 333,
  ) {
  }

  public function __invoke(): mixed {
    return $this->pintoBuild(function (Build $build): Build {
      return $build
        // `number` is not set.
        ->set('text', $this->text);
    });
  }

}
