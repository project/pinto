<?php

declare(strict_types=1);

namespace Drupal\pinto_test\Pinto\Slots;

use Drupal\pinto\Object\DrupalObjectTrait;
use Pinto\Attribute\ObjectType\Slots;
use Pinto\Slots\Build;

/**
 * Tests slots type.
 */
#[Slots]
final class SlotsObjectClass {

  use DrupalObjectTrait;

  /**
   * Constructor.
   */
  public function __construct(
    readonly string $text,
    readonly int $number = 3,
  ) {
  }

  public function __invoke(): mixed {
    return $this->pintoBuild(function (Build $build): Build {
      return $build
        ->set('text', $this->text)
        ->set('number', $this->number);
    });
  }

}
