<?php

declare(strict_types=1);

namespace Drupal\pinto_test\Pinto\Slots;

use Drupal\pinto\Object\DrupalObjectTrait;
use Drupal\pinto_test\Etc\SlotEnum;
use Pinto\Attribute\ObjectType\Slots;
use Pinto\Slots\Build;

/**
 * Slots defined on the attribute, an enum class-string is provided.
 */
#[Slots(
  slots: [
    SlotEnum::class,
  ],
)]
final class SlotsObjectExplicitEnumClass {

  use DrupalObjectTrait;

  /**
   * Constructor.
   */
  public function __construct() {
  }

  public function __invoke(): mixed {
    return $this->pintoBuild(static function (Build $build): Build {
      return $build
        ->set(SlotEnum::Slot1, 'Slot One')
        ->set(SlotEnum::Slot2, 'Slot Two')
        ->set(SlotEnum::Slot3, 'Slot Three');
    });
  }

}
