<?php

declare(strict_types=1);

namespace Drupal\pinto_test\Pinto\Slots;

use Pinto\Attribute\Definition;
use Pinto\List\ObjectListInterface;
use Pinto\List\ObjectListTrait;
use function Safe\realpath;

/**
 * Defines objects to test slots.
 */
enum Slots implements ObjectListInterface {

  use ObjectListTrait;

  #[Definition(SlotsObjectClass::class)]
  case SlotsObjectClass;

  #[Definition(SlotsObjectDefaultValue::class)]
  case SlotsObjectDefaultValue;

  #[Definition(SlotsObjectBindPromotedPublic::class)]
  case SlotsObjectBindPromotedPublic;

  #[Definition(SlotsObjectExplicit::class)]
  case SlotsObjectExplicit;

  #[Definition(SlotsObjectExplicitEnums::class)]
  case SlotsObjectExplicitEnums;

  #[Definition(SlotsObjectExplicitEnumClass::class)]
  case SlotsObjectExplicitEnumClass;

  public function templateName(): string {
    // Replace capital letters with hyphen + lowercase version of letter.
    // Then remove leading hyphen.
    // e.g: 'FooBar' -> 'foo-bar'.
    return \trim(\strtolower(\preg_replace('/([A-Z])/', '-$1', $this->name()) ?? ''), '-');
  }

  public function templateDirectory(): string {
    return '@pinto_test/templates/slots/';
  }

  public function cssDirectory(): string {
    return \substr(realpath(__DIR__ . '/../../../css'), \strlen(\DRUPAL_ROOT));
  }

  public function jsDirectory(): string {
    return \substr(realpath(__DIR__ . '/../../../js'), \strlen(\DRUPAL_ROOT));
  }

}
