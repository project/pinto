<?php

declare(strict_types=1);

namespace Drupal\pinto_test\Pinto\Slots;

use Drupal\pinto\Object\DrupalObjectTrait;
use Pinto\Attribute\ObjectType\Slots;
use Pinto\Slots\Build;

/**
 * Tests values bound from public promoted property.
 */
#[Slots(bindPromotedProperties: TRUE)]
final class SlotsObjectBindPromotedPublic {

  use DrupalObjectTrait;

  /**
   * Constructor.
   */
  public function __construct(
    public readonly string $pub,
  ) {
  }

  public function __invoke(): mixed {
    return $this->pintoBuild(static function (Build $build): Build {
      return $build;
    });
  }

}
