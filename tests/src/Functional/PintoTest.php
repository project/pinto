<?php

declare(strict_types=1);

namespace Drupal\Tests\pinto\Functional;

use Drupal\Core\Url;
use Drupal\pinto_test\Pinto\Slots\Slots;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests Pinto rendering.
 *
 * @group pinto
 */
final class PintoTest extends BrowserTestBase {

  protected $defaultTheme = 'stark';

  protected static $modules = [
    'pinto_test_routes',
    'pinto_test',
    'pinto',
  ];

  /**
   * Test direct invocation of a theme object.
   *
   * @see \Drupal\pinto_test_routes\Controller\ObjectTestController::__invoke
   * @see \Drupal\pinto_test\Pinto\Generic\Generic
   * @see \Drupal\pinto_test\Pinto\Generic\Objects
   */
  public function testDirectInvocation(): void {
    $this->drupalGet(Url::fromRoute('pinto_test.object_test'));
    $this->assertSession()->responseContains('Test text is foo bar!.');
    $this->assertSession()->responseContains('pinto_test/css/styles.css');
    $this->assertSession()->responseContains('pinto_test/js/app.js');
  }

  /**
   * Test directory based.
   *
   * @see \Drupal\pinto_test_routes\Controller\ObjectDirBased
   * @see \Drupal\pinto_test\Pinto\DirectoryBased\ObjectDirBased
   * @see \Drupal\pinto_test\Pinto\DirectoryBased\DirectoryBased
   */
  public function testDirBased(): void {
    $this->drupalGet(Url::fromRoute('pinto_test.dir_based'));
    $this->assertSession()->responseContains('Dir based template for foo bar!.');
    $this->assertSession()->responseContains('pinto_test/resources/Object_Dir_Based/styles.css');
    $this->assertSession()->responseContains('pinto_test/resources/Object_Dir_Based/app.js');
  }

  /**
   * Test directory based on theme object location.
   *
   * @see \Drupal\pinto_test_routes\Controller\DirBasedWithPhpController::__invoke
   * @see \PintoResources\pinto_test\Object_Dir_Based_With_Php\ThemeObject
   * @see \Drupal\pinto_test\Pinto\DirectoryBasedWithPhp\DirectoryBasedWithPhp
   */
  public function testDirBasedOnThemeObjectLocation(): void {
    $this->drupalGet(Url::fromRoute('pinto_test.dir_based_object_location'));
    $this->assertSession()->responseContains('Dir based template w/ PHP for foo bar!.');
    $this->assertSession()->responseContains('pinto_test/resources/Object_Dir_Based_With_Php/app.js');
    $this->assertSession()->responseContains('pinto_test/resources/Object_Dir_Based_With_Php/styles.css');
  }

  /**
   * Test nested rendering where inner object is predefined in the outer object.
   *
   * @see \Drupal\pinto_test_routes\Controller\NestedPredefinedController::__invoke
   * @see \Drupal\pinto_test\Pinto\Nested\ObjectNested
   * @see \Drupal\pinto_test\Pinto\Nested\ObjectNestedInner
   * @see \Drupal\pinto_test\Pinto\Nested\NestedObjects
   */
  public function testNestedPredefined(): void {
    $this->drupalGet(Url::fromRoute('pinto_test.nested_predefined'));
    $this->assertSession()->responseContains('Start outer inner [<span>[Start Inner [Nested inner text!] End Inner]</span>] End outer inner.');
  }

  /**
   * Test nested rendering where inner object is passed into the outer object.
   *
   * @see \Drupal\pinto_test_routes\Controller\NestedObjectController::__invoke
   * @see \Drupal\pinto_test\Pinto\Nested\ObjectNestedViaVariable
   * @see \Drupal\pinto_test\Pinto\Nested\ObjectNestedInner
   * @see \Drupal\pinto_test\Pinto\Nested\NestedObjects
   */
  public function testNestedObject(): void {
    $this->drupalGet(Url::fromRoute('pinto_test.nested_object'));
    $this->assertSession()->responseContains('Start variable wrapped outer inner [<span>[Start Inner [Nested inner text in a nested variable!] End Inner]</span>] End variable wrapped outer inner.');
  }

  /**
   * Tests slots.
   *
   * @see \Drupal\pinto_test_routes\Controller\SlotsClassObjectController::__invoke
   * @see \Drupal\pinto_test\Pinto\Slots\SlotsObjectClass
   * @see \Drupal\pinto_test\Pinto\Slots\Slots::SlotsObjectClass
   */
  public function testSlotsObjectClass(): void {
    $this->drupalGet(Url::fromRoute('pinto_test.slots.object_class'));
    $this->assertSession()->pageTextContains('Text: Text! Number: 12345');
  }

  /**
   * Tests slots default values.
   *
   * @see \Drupal\pinto_test_routes\Controller\SlotsClassObjectDefaultValueController::__invoke
   * @see \Drupal\pinto_test\Pinto\Slots\SlotsObjectDefaultValue
   * @see \Drupal\pinto_test\Pinto\Slots\Slots::SlotsObjectDefaultValue
   */
  public function testSlotsObjectDefaultValue(): void {
    /** @var array<string, mixed> $hookTheme */
    $hookTheme = \Drupal::getContainer()->getParameter('pinto.internal.hook_theme');
    static::assertEquals([
      'variables' => [
        'text' => NULL,
        'number' => 333,
      ],
      'path' => '@pinto_test/templates/slots/',
      'template' => 'slots-object-default-value',
    ], $hookTheme[Slots::SlotsObjectDefaultValue->name]);
    $this->drupalGet(Url::fromRoute('pinto_test.slots.object_default_value'));
    $this->assertSession()->pageTextContains('Text: Text! Number: 333');
  }

  /**
   * Tests slots defined in the attribute.
   *
   * @see \Drupal\pinto_test_routes\Controller\SlotsObjectExplicitController::__invoke
   * @see \Drupal\pinto_test\Pinto\Slots\SlotsObjectExplicit
   * @see \Drupal\pinto_test\Pinto\Slots\Slots::SlotsObjectExplicit
   */
  public function testSlotsObjectExplicit(): void {
    $this->drupalGet(Url::fromRoute('pinto_test.slots.object_explicit'));
    $this->assertSession()->pageTextContains('Some text | 12345');
  }

  /**
   * Tests slots defined in the attribute, where slots are enums.
   *
   * @see \Drupal\pinto_test_routes\Controller\SlotsObjectExplicitEnumsController::__invoke
   * @see \Drupal\pinto_test\Pinto\Slots\SlotsObjectExplicitEnums
   * @see \Drupal\pinto_test\Pinto\Slots\Slots::SlotsObjectExplicitEnums
   */
  public function testSlotsObjectExplicitEnums(): void {
    $this->drupalGet(Url::fromRoute('pinto_test.slots.object_explicit_enums'));
    $this->assertSession()->pageTextContains('Slot One | 23456');
  }

  /**
   * Tests slots defined in the attribute, where slots are enums.
   *
   * @see \Drupal\pinto_test_routes\Controller\SlotsObjectBindPromotedPublicController::__invoke
   * @see \Drupal\pinto_test\Pinto\Slots\SlotsObjectBindPromotedPublic
   * @see \Drupal\pinto_test\Pinto\Slots\Slots::SlotsObjectBindPromotedPublic
   */
  public function testSlotsObjectBindPromotedPublic(): void {
    $this->drupalGet(Url::fromRoute('pinto_test.slots.object_bind_promoted_public'));
    $this->assertSession()->pageTextContains('Public text!');
  }

  /**
   * Tests slots defined in the attribute, where slots are enum class-strings.
   *
   * @see \Drupal\pinto_test_routes\Controller\SlotsObjectExplicitEnumClassController::__invoke
   * @see \Drupal\pinto_test\Pinto\Slots\SlotsObjectExplicitEnumClass
   * @see \Drupal\pinto_test\Pinto\Slots\Slots::SlotsObjectExplicitEnumClass
   */
  public function testSlotsObjectExplicitEnumClass(): void {
    $this->drupalGet(Url::fromRoute('pinto_test.slots.object_explicit_enum_class'));
    $this->assertSession()->pageTextContains('Slot One | Slot Two | Slot Three');
  }

}
