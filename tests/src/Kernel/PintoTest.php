<?php

declare(strict_types=1);

namespace Drupal\Tests\pinto\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\pinto_test\Other\OtherObject;
use Drupal\pinto_test\Pinto\Generic\Objects;

/**
 * Tests Pinto.
 *
 * @group pinto
 */
final class PintoTest extends KernelTestBase {

  protected static $modules = [
    'pinto_test',
    'pinto',
  ];

  /**
   * Test objects in definitions.
   *
   * @see \Drupal\pinto_test\Pinto\Generic\ObjectThemeDefinitionClass
   */
  public function testObjectInDefinition(): void {
    /** @var array<string, mixed> $hookTheme */
    $hookTheme = \Drupal::getContainer()->getParameter('pinto.internal.hook_theme');
    static::assertEquals([
      'variables' => [
        'foo' => NULL,
        'other' => new OtherObject(),
      ],
      'path' => '@pinto_test/templates/',
      'template' => 'theme-definition-on-class',
    ], $hookTheme[Objects::ObjectThemeDefinitionClass->value]);
  }

}
