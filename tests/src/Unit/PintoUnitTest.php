<?php

declare(strict_types=1);

namespace Drupal\Tests\pinto\Unit;

use Drupal\pinto\Object\PintoToDrupalBuilder;
use Drupal\pinto_test\Etc\SlotEnum;
use Drupal\Tests\UnitTestCase;

/**
 * Tests Pinto.
 *
 * @group pinto
 */
final class PintoUnitTest extends UnitTestCase {

  /**
   * @covers \Drupal\pinto\Object\PintoToDrupalBuilder::unitEnumToHookThemeVariableName
   */
  public function testUnitEnumToHookThemeVariableName(): void {
    static::assertEquals('fooBarBaz', PintoToDrupalBuilder::unitEnumToHookThemeVariableName('$fooBar%Baz'));
    static::assertEquals('Slot1', PintoToDrupalBuilder::unitEnumToHookThemeVariableName(SlotEnum::Slot1));
  }

}
